package httpserver

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type contextKey int

const (
	claimContextKey contextKey = 1
)

type Claims struct {
	jwt.StandardClaims
	UserID   int64
	UserType string
}

type Jwt struct {
	appName       string
	LoginExp      time.Duration
	signingMethod *jwt.SigningMethodHMAC
	signatureKey  []byte
}

func NewJwt(signatureKey string, expireTime time.Duration) *Jwt {
	return &Jwt{
		appName:       "appointment-coach",
		LoginExp:      expireTime,
		signingMethod: jwt.SigningMethodHS256,
		signatureKey:  []byte(signatureKey),
	}
}

func (j *Jwt) GenerateAPIToken(userId int64, userType string) (string, error) {
	c := Claims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    j.appName,
			ExpiresAt: time.Now().Add(j.LoginExp).Unix(),
		},
		UserID:   userId,
		UserType: userType,
	}

	token := jwt.NewWithClaims(
		j.signingMethod,
		c,
	)

	signedToken, err := token.SignedString(j.signatureKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func (j *Jwt) Decode(ctx context.Context, tokenJwt string) (Claims, error) {
	token, err := jwt.Parse(tokenJwt, func(token *jwt.Token) (interface{}, error) {

		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return false, fmt.Errorf("invalid Sign Method")
		} else if method != j.signingMethod {
			return false, fmt.Errorf("invalid Sign Method")
		}

		return j.signatureKey, nil
	})
	if err != nil {
		err = fmt.Errorf("(%w) Decode - jwt.Parse error : %s", fmt.Errorf("Forbidden"), err.Error())
		return Claims{}, err
	}

	mapClaims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		err = fmt.Errorf("(%w) !ok || !token.Valid", fmt.Errorf("Forbidden"))
		return Claims{}, err
	}

	byteClaims, err := json.Marshal(mapClaims)
	if err != nil {
		err = fmt.Errorf("Decode - json.Marshal(mapClaims) : %w", err)
		return Claims{}, err
	}

	var claims Claims
	err = json.Unmarshal(byteClaims, &claims)
	if err != nil {
		err = fmt.Errorf("Decode - json.Unmarshal(byteClaims, &claims) : %w", err)
		return Claims{}, err
	}

	return claims, nil
}

func (j *Jwt) AuthorizeAccess(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenSent := r.Header.Get("Authorization")
		if tokenSent == "" {
			WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": "token not found"}, w)
			return
		}

		claims, err := j.Decode(r.Context(), tokenSent)
		if err != nil {
			WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": err.Error()}, w)
			return
		}

		ctx := context.WithValue(r.Context(), claimContextKey, claims)
		*r = *r.Clone(ctx)

		next(w, r)
	}
}
