package httpserver

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yoratyo/appointment-coach/usecase"
)

type fakeAPI struct {
	GetUserFunc               func(ctx context.Context, userId int64) (usecase.GetUserResponse, error)
	MakeAppointmentFunc       func(ctx context.Context, req usecase.MakeAppointmentRequest) (usecase.MakeAppointmentResponse, error)
	CancelAppointmentFunc     func(ctx context.Context, req usecase.CancelAppointmentRequest) error
	RescheduleAppointmentFunc func(ctx context.Context, req usecase.RescheduleAppointmentRequest) error
}

func (a *fakeAPI) GetUser(ctx context.Context, userId int64) (usecase.GetUserResponse, error) {
	return a.GetUserFunc(ctx, userId)
}

func (a *fakeAPI) MakeAppointment(ctx context.Context, req usecase.MakeAppointmentRequest) (usecase.MakeAppointmentResponse, error) {
	return a.MakeAppointmentFunc(ctx, req)
}

func (a *fakeAPI) CancelAppointment(ctx context.Context, req usecase.CancelAppointmentRequest) error {
	return a.CancelAppointmentFunc(ctx, req)
}

func (a *fakeAPI) RescheduleAppointment(ctx context.Context, req usecase.RescheduleAppointmentRequest) error {
	return a.RescheduleAppointmentFunc(ctx, req)
}

func Test_handler_getToken(t *testing.T) {
	newServer := func(t *testing.T, api *fakeAPI) *httptest.Server {
		s := New(api, ":8080", "some-jwt-key", 1*time.Hour)
		st := httptest.NewServer(s.Handler)
		t.Cleanup(st.Close)

		return st
	}

	newReq := func(host, userId string) *http.Request {
		r, _ := http.NewRequest(http.MethodGet, host+"/auth/get-token/"+userId, nil)

		return r
	}

	t.Run("success", func(t *testing.T) {
		api := &fakeAPI{
			GetUserFunc: func(ctx context.Context, userId int64) (usecase.GetUserResponse, error) {
				return usecase.GetUserResponse{UserID: 1, UserType: usecase.CLIENT_USER_TYPE}, nil
			},
		}
		s := newServer(t, api)
		req := newReq(s.URL, "1")
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Contains(t, string(b), `"token":`)
		assert.Equal(t, http.StatusOK, resp.StatusCode)
	})

	t.Run("error invalid userId", func(t *testing.T) {
		api := &fakeAPI{
			GetUserFunc: func(ctx context.Context, userId int64) (usecase.GetUserResponse, error) {
				return usecase.GetUserResponse{UserID: 1, UserType: usecase.CLIENT_USER_TYPE}, nil
			},
		}
		s := newServer(t, api)
		req := newReq(s.URL, "invalid")
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"invalid userId: strconv.ParseInt: parsing \"invalid\": invalid syntax"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error usecase get user", func(t *testing.T) {
		api := &fakeAPI{
			GetUserFunc: func(ctx context.Context, userId int64) (usecase.GetUserResponse, error) {
				return usecase.GetUserResponse{}, sql.ErrNoRows
			},
		}
		s := newServer(t, api)
		req := newReq(s.URL, "1")
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error usecase: sql: no rows in result set"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})
}

func Test_handler_createAppointment(t *testing.T) {
	newServer := func(t *testing.T, api *fakeAPI) *httptest.Server {
		s := New(api, ":8080", "some-jwt-key", 1*time.Hour)
		st := httptest.NewServer(s.Handler)
		t.Cleanup(st.Close)

		return st
	}

	newReq := func(host string, req interface{}) *http.Request {
		b, _ := json.Marshal(req)
		r, _ := http.NewRequest(http.MethodPost, host+"/v1/create-appointment", bytes.NewReader(b))

		j := *NewJwt("some-jwt-key", 1*time.Hour)
		token, _ := j.GenerateAPIToken(1, usecase.CLIENT_USER_TYPE)
		r.Header.Set("Authorization", token)

		return r
	}

	t.Run("success", func(t *testing.T) {
		api := &fakeAPI{
			MakeAppointmentFunc: func(ctx context.Context, req usecase.MakeAppointmentRequest) (usecase.MakeAppointmentResponse, error) {
				return usecase.MakeAppointmentResponse{AppointmentID: 1}, nil
			},
		}
		s := newServer(t, api)
		body := createAppointmentRequest{
			CoachUserID: 1,
			Time:        "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"appointment_id":1},"status":"ok"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusOK, resp.StatusCode)
	})

	t.Run("error parse body", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := "wrong body"
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error parse request: json: cannot unmarshal string into Go value of type httpserver.createAppointmentRequest"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error parse booking time", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := createAppointmentRequest{
			CoachUserID: 1,
			Time:        "12",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error parse time: parsing time \"12\" as \"2006-01-02T15:04:05Z07:00\": cannot parse \"12\" as \"2006\""},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error invalid body value", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := createAppointmentRequest{
			CoachUserID: 0,
			Time:        "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"invalid request"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error usecase", func(t *testing.T) {
		e := errors.New("failed")
		api := &fakeAPI{
			MakeAppointmentFunc: func(ctx context.Context, req usecase.MakeAppointmentRequest) (usecase.MakeAppointmentResponse, error) {
				return usecase.MakeAppointmentResponse{}, e
			},
		}
		s := newServer(t, api)
		body := createAppointmentRequest{
			CoachUserID: 1,
			Time:        "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error usecase: failed"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})
}

func Test_handler_cancelAppointment(t *testing.T) {
	newServer := func(t *testing.T, api *fakeAPI) *httptest.Server {
		s := New(api, ":8080", "some-jwt-key", 1*time.Hour)
		st := httptest.NewServer(s.Handler)
		t.Cleanup(st.Close)

		return st
	}

	newReq := func(host string, req interface{}) *http.Request {
		b, _ := json.Marshal(req)
		r, _ := http.NewRequest(http.MethodPost, host+"/v1/cancel-appointment", bytes.NewReader(b))

		j := *NewJwt("some-jwt-key", 1*time.Hour)
		token, _ := j.GenerateAPIToken(1, usecase.CLIENT_USER_TYPE)
		r.Header.Set("Authorization", token)

		return r
	}

	t.Run("success", func(t *testing.T) {
		api := &fakeAPI{
			CancelAppointmentFunc: func(ctx context.Context, req usecase.CancelAppointmentRequest) error {
				return nil
			},
		}
		s := newServer(t, api)
		body := cancelAppointmentRequest{
			AppointmentID: 1,
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"success cancel"},"status":"ok"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusOK, resp.StatusCode)
	})

	t.Run("error parse body", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := "cancel"
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error parse request: json: cannot unmarshal string into Go value of type httpserver.cancelAppointmentRequest"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error invalid body value", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := cancelAppointmentRequest{
			AppointmentID: 0,
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"invalid request"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error usecase", func(t *testing.T) {
		e := errors.New("failed")
		api := &fakeAPI{
			CancelAppointmentFunc: func(ctx context.Context, req usecase.CancelAppointmentRequest) error {
				return e
			},
		}
		s := newServer(t, api)
		body := cancelAppointmentRequest{
			AppointmentID: 1,
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error usecase: failed"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})
}

func Test_handler_RescheduleAppointment(t *testing.T) {
	newServer := func(t *testing.T, api *fakeAPI) *httptest.Server {
		s := New(api, ":8080", "some-jwt-key", 1*time.Hour)
		st := httptest.NewServer(s.Handler)
		t.Cleanup(st.Close)

		return st
	}

	newReq := func(host string, req interface{}) *http.Request {
		b, _ := json.Marshal(req)
		r, _ := http.NewRequest(http.MethodPost, host+"/v1/reschedule-appointment", bytes.NewReader(b))

		j := *NewJwt("some-jwt-key", 1*time.Hour)
		token, _ := j.GenerateAPIToken(1, usecase.CLIENT_USER_TYPE)
		r.Header.Set("Authorization", token)

		return r
	}

	t.Run("success", func(t *testing.T) {
		api := &fakeAPI{
			RescheduleAppointmentFunc: func(ctx context.Context, req usecase.RescheduleAppointmentRequest) error {
				return nil
			},
		}
		s := newServer(t, api)
		body := rescheduleAppointmentRequest{
			AppointmentID: 1,
			Time:          "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"success reschedule"},"status":"ok"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusOK, resp.StatusCode)
	})

	t.Run("error parse body", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := "reschedule"
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error parse request: json: cannot unmarshal string into Go value of type httpserver.rescheduleAppointmentRequest"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error parse time", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := rescheduleAppointmentRequest{
			AppointmentID: 1,
			Time:          "12",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error parse time: parsing time \"12\" as \"2006-01-02T15:04:05Z07:00\": cannot parse \"12\" as \"2006\""},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error invalid body value", func(t *testing.T) {
		s := newServer(t, &fakeAPI{})
		body := rescheduleAppointmentRequest{
			AppointmentID: 0,
			Time:          "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"invalid request"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})

	t.Run("error usecase", func(t *testing.T) {
		e := errors.New("failed")
		api := &fakeAPI{
			RescheduleAppointmentFunc: func(ctx context.Context, req usecase.RescheduleAppointmentRequest) error {
				return e
			},
		}
		s := newServer(t, api)
		body := rescheduleAppointmentRequest{
			AppointmentID: 1,
			Time:          "2022-09-26T09:30:00+08:00",
		}
		req := newReq(s.URL, body)
		resp, _ := http.DefaultClient.Do(req)

		b, _ := io.ReadAll(resp.Body)
		assert.Equal(t, `{"data":{"message":"error usecase: failed"},"status":"error"}`, strings.TrimSpace(string(b)))
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})
}
