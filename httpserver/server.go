package httpserver

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/yoratyo/appointment-coach/usecase"
)

func New(api API, addr, signatureKey string, tokenMaxAge time.Duration) *http.Server {
	h := &handler{
		api: api,
		jwt: *NewJwt(signatureKey, tokenMaxAge),
	}

	r := mux.NewRouter()
	r.Use(WithLogging)

	auth := r.PathPrefix("/auth").Subrouter()
	auth.HandleFunc("/get-token/{userId}", h.getToken).Methods(http.MethodGet)

	rv1 := r.PathPrefix("/v1").Subrouter()
	rv1.HandleFunc("/create-appointment", h.jwt.AuthorizeAccess(h.createAppointment)).Methods(http.MethodPost)
	rv1.HandleFunc("/cancel-appointment", h.jwt.AuthorizeAccess(h.cancelAppointment)).Methods(http.MethodPost)
	rv1.HandleFunc("/reschedule-appointment", h.jwt.AuthorizeAccess(h.RescheduleAppointment)).Methods(http.MethodPost)

	return &http.Server{
		Addr:    addr,
		Handler: r,
	}
}

type API interface {
	GetUser(ctx context.Context, userId int64) (usecase.GetUserResponse, error)
	MakeAppointment(ctx context.Context, req usecase.MakeAppointmentRequest) (usecase.MakeAppointmentResponse, error)
	CancelAppointment(ctx context.Context, req usecase.CancelAppointmentRequest) error
	RescheduleAppointment(ctx context.Context, req usecase.RescheduleAppointmentRequest) error
}

type handler struct {
	api API
	jwt Jwt
}

func (h *handler) getToken(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["userId"]
	userId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("invalid userId: %s", err.Error())}, w)
		return
	}

	usr, err := h.api.GetUser(r.Context(), userId)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error usecase: %s", err.Error())}, w)
		return
	}

	token, err := h.jwt.GenerateAPIToken(int64(usr.UserID), usr.UserType)
	if err != nil {
		WriteJSONResponse(http.StatusInternalServerError, map[string]string{"message": fmt.Sprintf("error generate token: %s", err.Error())}, w)
		return
	}

	WriteJSONResponse(http.StatusOK, map[string]string{"token": token}, w)
}

type createAppointmentRequest struct {
	CoachUserID uint64 `json:"coach_user_id"`
	Time        string `json:"time"`
	Notes       string `json:"notes"`
}

func (h *handler) createAppointment(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	claims, _ := ClaimFromContext(ctx)

	reqBody, _ := ioutil.ReadAll(r.Body)
	var req createAppointmentRequest
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error parse request: %s", err.Error())}, w)
		return
	}

	bookingTime, err := time.Parse(time.RFC3339, req.Time)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error parse time: %s", err.Error())}, w)
		return
	}

	if req.CoachUserID == 0 || bookingTime.IsZero() {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": "invalid request"}, w)
		return
	}

	res, err := h.api.MakeAppointment(ctx, usecase.MakeAppointmentRequest{
		ClientUserID: claims.UserID,
		UserType:     claims.UserType,
		CoachUserID:  int64(req.CoachUserID),
		BookingTime:  bookingTime,
		Notes:        req.Notes,
	})
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error usecase: %s", err.Error())}, w)
		return
	}

	WriteJSONResponse(http.StatusOK, map[string]uint64{"appointment_id": res.AppointmentID}, w)
}

type cancelAppointmentRequest struct {
	AppointmentID uint64 `json:"appointment_id"`
}

func (h *handler) cancelAppointment(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	claims, _ := ClaimFromContext(ctx)

	reqBody, _ := ioutil.ReadAll(r.Body)
	var req cancelAppointmentRequest
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error parse request: %s", err.Error())}, w)
		return
	}

	if req.AppointmentID == 0 {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": "invalid request"}, w)
		return
	}

	err = h.api.CancelAppointment(ctx, usecase.CancelAppointmentRequest{
		UserID:        claims.UserID,
		UserType:      claims.UserType,
		AppointmentID: req.AppointmentID,
	})
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error usecase: %s", err.Error())}, w)
		return
	}

	WriteJSONResponse(http.StatusOK, map[string]string{"message": "success cancel"}, w)
}

type rescheduleAppointmentRequest struct {
	AppointmentID uint64 `json:"appointment_id"`
	Time          string `json:"time"`
}

func (h *handler) RescheduleAppointment(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	claims, _ := ClaimFromContext(ctx)

	reqBody, _ := ioutil.ReadAll(r.Body)
	var req rescheduleAppointmentRequest
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error parse request: %s", err.Error())}, w)
		return
	}

	bookingTime, err := time.Parse(time.RFC3339, req.Time)
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error parse time: %s", err.Error())}, w)
		return
	}

	if req.AppointmentID == 0 || bookingTime.IsZero() {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": "invalid request"}, w)
		return
	}

	err = h.api.RescheduleAppointment(ctx, usecase.RescheduleAppointmentRequest{
		UserID:        claims.UserID,
		UserType:      claims.UserType,
		AppointmentID: req.AppointmentID,
		BookingTime:   bookingTime,
	})
	if err != nil {
		WriteJSONResponse(http.StatusBadRequest, map[string]string{"message": fmt.Sprintf("error usecase: %s", err.Error())}, w)
		return
	}

	WriteJSONResponse(http.StatusOK, map[string]string{"message": "success reschedule"}, w)
}

func ClaimFromContext(ctx context.Context) (Claims, error) {
	claims, ok := ctx.Value(claimContextKey).(Claims)
	if !ok {
		return Claims{}, errors.New("nil claims")
	}

	if claims.UserID == 0 {
		return Claims{}, errors.New("UserID claim not found")
	}

	return claims, nil
}

func WriteJSONResponse(statusCode int, data interface{}, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	d := make(map[string]interface{})
	d["data"] = data

	if statusCode == http.StatusOK {
		d["status"] = "ok"
	} else {
		d["status"] = "error"
	}
	json.NewEncoder(w).Encode(d)
}
