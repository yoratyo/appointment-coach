package config

import (
	"log"
	"time"

	"github.com/kelseyhightower/envconfig"
)

var Env struct {
	AppPort      string        `envconfig:"APP_PORT" required:"true"`
	AuthJWTKey   string        `envconfig:"AUTH_JWT_KEY" required:"true"`
	JWTKeyMaxAge time.Duration `envconfig:"JWT_KEY_MAX_AGE" required:"true"`
	SlotDuration time.Duration `envconfig:"SLOT_DURATION" required:"true"`

	PostgresHost     string `envconfig:"POSTGRES_HOST" required:"true"`
	PostgresUser     string `envconfig:"POSTGRES_USER" required:"true"`
	PostgresPassword string `envconfig:"POSTGRES_PASSWORD" required:"true"`
	PostgresDBName   string `envconfig:"POSTGRES_DBNAME" required:"true"`
}

func init() {
	if err := envconfig.Process("APPOINTMENT", &Env); err != nil {
		log.Fatal(err.Error())
	}
}
