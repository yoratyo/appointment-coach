# appointment-coach

## Coverage

| Branch        | Pipeline          | Code coverage  |
| ------------- |:-----------------:| --------------:|
| main       | [![pipeline status](https://gitlab.com/yoratyo/appointment-coach/badges/main/pipeline.svg)](https://gitlab.com/yoratyo/appointment-coach/-/commits/main)  | [![coverage report](https://gitlab.com/yoratyo/appointment-coach/badges/main/coverage.svg)](https://gitlab.com/yoratyo/appointment-coach/-/commits/main) |

## Introduction

The appointment-coach provide feature for user can make an appointment with coach if only coach didn't have another appointment and have an availability. 

## Assumption

The system has some assumption and scope bellow:
- All feature endpoints need JWT token for each user, use endpoint `GET /auth/get-token/{userId}` to simulate auth process
- There is two type of user (`COACH` & `CLIENT`)
- Each appointment slot have max time set on `.env`, eq: 30 minutes
- Timezone type for coach schedule define IANA timezone data format, https://gist.github.com/aviflax/a4093965be1cd008f172
- Format time to create and reschedule appointment is using RFC3339 format, eq: `2022-09-26T09:30:00+08:00`
- Client can book and only cancel for their own appointment
- Coach can only cancel and reschedule their own appointment

## Installation

There is two ways to run the project,

1. Using docker
    - set environmet value in `.env` file
    - run command,
    ```
    docker-compose up --build
    ```

2. Without docker
    - set up postgres database on your local
    - set environmet value in `.env` file
    - run command,
    ```
    go run main.go
    ``` 
    , or build
    ```
    go build -o /run
    /run
    ```

## API

This is the list endpoint of appointment-coach:



| Path | Description |
| :--- | :--- |
| ```GET /auth/get-token/{userId}``` | generate token by userId to simulate auth |
| ```POST /v1/create-appointment``` | only client can create new appointment, require header `Authorization` with value from generate token |
| ```POST /v1/cancel-appointment``` | client & coach can cancel their appointment, require header `Authorization` with value from generate token |
| ```POST /v1/reschedule-appointment``` | only coach can reschedule their appointment, require header `Authorization` with value from generate token |