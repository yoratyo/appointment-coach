package usecase

import (
	"context"
	"database/sql"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yoratyo/appointment-coach/repository"
)

type fakeAppointmentDB struct {
	GetUserByIdFunc        func(ctx context.Context, id int64) (repository.User, error)
	GetCoachScheduleFunc   func(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error)
	CreateAppointmentFunc  func(ctx context.Context, a repository.Appointment) (uint64, error)
	GetAppointmentByIdFunc func(ctx context.Context, id int64) (repository.Appointment, error)
	UpdateAppointmentFunc  func(ctx context.Context, a repository.Appointment) error
}

func (d *fakeAppointmentDB) GetUserById(ctx context.Context, id int64) (repository.User, error) {
	return d.GetUserByIdFunc(ctx, id)
}

func (d *fakeAppointmentDB) GetCoachSchedule(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error) {
	return d.GetCoachScheduleFunc(ctx, coach_user_id)
}

func (d *fakeAppointmentDB) CreateAppointment(ctx context.Context, a repository.Appointment) (uint64, error) {
	return d.CreateAppointmentFunc(ctx, a)
}

func (d *fakeAppointmentDB) GetAppointmentById(ctx context.Context, id int64) (repository.Appointment, error) {
	return d.GetAppointmentByIdFunc(ctx, id)
}

func (d *fakeAppointmentDB) UpdateAppointment(ctx context.Context, a repository.Appointment) error {
	return d.UpdateAppointmentFunc(ctx, a)
}

func TestAppointmentAPI_GetUser(t *testing.T) {
	newAPI := func(user repository.User) *AppointmentAPI {
		fdb := &fakeAppointmentDB{
			GetUserByIdFunc: func(ctx context.Context, id int64) (repository.User, error) {
				return user, nil
			},
		}

		return NewAppointmentAPI(
			fdb,
			30*time.Minute,
		)
	}

	t.Run("success", func(t *testing.T) {
		user := repository.User{
			ID:       1,
			Name:     "test",
			UserType: "COACH",
		}
		expected := GetUserResponse{
			UserID:   user.ID,
			Name:     user.Name,
			Email:    user.Email,
			UserType: user.UserType,
		}

		api := newAPI(user)
		resp, err := api.GetUser(context.Background(), int64(user.ID))

		assert.Equal(t, expected, resp)
		assert.Nil(t, err)
	})

	t.Run("error get from db", func(t *testing.T) {
		usr := repository.User{ID: 1}
		api := newAPI(usr)
		e := errors.New("error")
		api.db.(*fakeAppointmentDB).GetUserByIdFunc = func(ctx context.Context, id int64) (repository.User, error) {
			return usr, e
		}

		resp, err := api.GetUser(context.Background(), int64(usr.ID))

		assert.Equal(t, GetUserResponse{}, resp)
		assert.ErrorIs(t, err, e)
	})
}

func TestAppointmentAPI_CheckCoachSchedule(t *testing.T) {
	newAPI := func(schedules []repository.CoachSchedule) *AppointmentAPI {
		fdb := &fakeAppointmentDB{
			GetCoachScheduleFunc: func(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error) {
				return schedules, nil
			},
		}

		return NewAppointmentAPI(
			fdb,
			30*time.Minute,
		)
	}

	t.Run("success", func(t *testing.T) {
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Asia/Jakarta",
				Day:         "Monday",
				StartTime:   "07:00AM",
				EndTime:     "10:00AM",
			},
		}
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")

		api := newAPI(schedules)
		found, err := api.CheckCoachSchedule(context.Background(),
			int64(schedules[0].CoachUserID), bookingTime,
		)

		assert.Equal(t, true, found)
		assert.Nil(t, err)
	})

	t.Run("not found schedule", func(t *testing.T) {
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Asia/Jakarta",
				Day:         "Sunday",
				StartTime:   "07:00AM",
				EndTime:     "10:00AM",
			},
		}
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")

		api := newAPI(schedules)
		found, err := api.CheckCoachSchedule(context.Background(),
			int64(schedules[0].CoachUserID), bookingTime,
		)

		assert.Equal(t, false, found)
		assert.Nil(t, err)
	})

	t.Run("error parsing timezone", func(t *testing.T) {
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Cilandak",
				Day:         "Monday",
				StartTime:   "07:00AM",
				EndTime:     "10:00AM",
			},
		}
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")

		api := newAPI(schedules)
		found, err := api.CheckCoachSchedule(context.Background(),
			int64(schedules[0].CoachUserID), bookingTime,
		)

		assert.Equal(t, false, found)
		assert.Equal(t, err.Error(), "parsing timezone schedule: unknown time zone Cilandak")
	})

	t.Run("error parsing start time", func(t *testing.T) {
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Asia/Jakarta",
				Day:         "Monday",
				StartTime:   "start",
				EndTime:     "10:00AM",
			},
		}
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")

		api := newAPI(schedules)
		found, err := api.CheckCoachSchedule(context.Background(),
			int64(schedules[0].CoachUserID), bookingTime,
		)

		assert.Equal(t, false, found)
		assert.Equal(t, err.Error(), `parsing start time schedule: parsing time "start" as "3:04PM": cannot parse "start" as "3"`)
	})

	t.Run("error parsing start time", func(t *testing.T) {
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Asia/Jakarta",
				Day:         "Monday",
				StartTime:   "07:00AM",
				EndTime:     "end",
			},
		}
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")

		api := newAPI(schedules)
		found, err := api.CheckCoachSchedule(context.Background(),
			int64(schedules[0].CoachUserID), bookingTime,
		)

		assert.Equal(t, false, found)
		assert.Equal(t, err.Error(), `parsing end time schedule: parsing time "end" as "3:04PM": cannot parse "end" as "3"`)
	})

	t.Run("error db get coach schedules", func(t *testing.T) {
		api := newAPI([]repository.CoachSchedule{})
		api.db.(*fakeAppointmentDB).GetCoachScheduleFunc = func(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error) {
			return []repository.CoachSchedule{}, sql.ErrNoRows
		}
		found, err := api.CheckCoachSchedule(context.Background(), 1, time.Time{})

		assert.Equal(t, false, found)
		assert.ErrorIs(t, err, sql.ErrNoRows)
	})
}

func TestAppointmentAPI_MakeAppointment(t *testing.T) {
	schedules := []repository.CoachSchedule{
		{
			ID:          1,
			CoachUserID: 1,
			Timezone:    "Asia/Jakarta",
			Day:         "Monday",
			StartTime:   "07:00AM",
			EndTime:     "10:00AM",
		},
	}
	newAPI := func(errGetSchedule, errCreateApt error) *AppointmentAPI {
		fdb := &fakeAppointmentDB{
			GetCoachScheduleFunc: func(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error) {
				return schedules, errGetSchedule
			},
			CreateAppointmentFunc: func(ctx context.Context, a repository.Appointment) (uint64, error) {
				return uint64(1), errCreateApt
			},
		}

		return NewAppointmentAPI(
			fdb,
			30*time.Minute,
		)
	}

	newReq := func(userType string, time time.Time) MakeAppointmentRequest {
		return MakeAppointmentRequest{
			ClientUserID: 1,
			CoachUserID:  1,
			UserType:     userType,
			Notes:        "notes",
			BookingTime:  time,
		}
	}

	t.Run("success", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")
		req := newReq(CLIENT_USER_TYPE, bookingTime)

		api := newAPI(nil, nil)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{AppointmentID: 1}, resp)
		assert.Nil(t, err)
	})

	t.Run("error user type not client", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")
		req := newReq(COACH_USER_TYPE, bookingTime)

		api := newAPI(nil, nil)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{}, resp)
		assert.EqualError(t, err, "invalid user type, must be client")
	})

	t.Run("error get coach schedule", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")
		req := newReq(CLIENT_USER_TYPE, bookingTime)

		api := newAPI(sql.ErrNoRows, nil)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{}, resp)
		assert.ErrorIs(t, err, sql.ErrNoRows)
	})

	t.Run("error schedule not match", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T17:30:00+08:00")
		req := newReq(CLIENT_USER_TYPE, bookingTime)

		api := newAPI(nil, nil)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{}, resp)
		assert.EqualError(t, err, "not valid slot")
	})

	t.Run("error parsing time", func(t *testing.T) {
		req := newReq(CLIENT_USER_TYPE, time.Time{})

		api := newAPI(nil, nil)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{}, resp)
		assert.EqualError(t, err, "invalid booking time")
	})

	t.Run("error db create appointment", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")
		req := newReq(CLIENT_USER_TYPE, bookingTime)
		e := errors.New("failed")

		api := newAPI(nil, e)
		resp, err := api.MakeAppointment(context.Background(), req)

		assert.Equal(t, MakeAppointmentResponse{}, resp)
		assert.ErrorIs(t, err, e)
	})
}

func TestAppointmentAPI_CancelAppointment(t *testing.T) {
	newAPI := func(aptStatus string, errGetApt, errUpdateApt error) *AppointmentAPI {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00Z")
		appointment := repository.Appointment{
			ID:           1,
			ClientUserID: 1,
			CoachUserID:  1,
			BookingTime:  bookingTime,
			Status:       aptStatus,
		}

		fdb := &fakeAppointmentDB{
			GetAppointmentByIdFunc: func(ctx context.Context, id int64) (repository.Appointment, error) {
				return appointment, errGetApt
			},
			UpdateAppointmentFunc: func(ctx context.Context, a repository.Appointment) error {
				return errUpdateApt
			},
		}

		return NewAppointmentAPI(
			fdb,
			30*time.Minute,
		)
	}

	newReq := func(userType string, userId, aptId int64) CancelAppointmentRequest {
		return CancelAppointmentRequest{
			UserID:        userId,
			UserType:      userType,
			AppointmentID: uint64(aptId),
		}
	}

	t.Run("success by client", func(t *testing.T) {
		req := newReq(CLIENT_USER_TYPE, 1, 1)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil)
		err := api.CancelAppointment(context.Background(), req)

		assert.Nil(t, err)
	})

	t.Run("success by coach", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 1, 1)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil)
		err := api.CancelAppointment(context.Background(), req)

		assert.Nil(t, err)
	})

	t.Run("error db get appointment", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 1, 1)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, sql.ErrNoRows, nil)
		err := api.CancelAppointment(context.Background(), req)

		assert.ErrorIs(t, err, sql.ErrNoRows)
	})

	t.Run("error appointment already canceled", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 1, 1)

		api := newAPI(APPOINTMENT_STATUS_CANCELED, nil, nil)
		err := api.CancelAppointment(context.Background(), req)

		assert.EqualError(t, err, "appointment already canceled")
	})

	t.Run("error forbidden user", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 2, 1)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil)
		err := api.CancelAppointment(context.Background(), req)

		assert.EqualError(t, err, "forbidden, appointment not belongs to you")
	})

	t.Run("error db update appointment", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 1, 1)
		e := errors.New("duplicate")

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, e)
		err := api.CancelAppointment(context.Background(), req)

		assert.ErrorIs(t, err, e)
	})
}

func TestAppointmentAPI_RescheduleAppointment(t *testing.T) {
	newAPI := func(aptStatus string, errGetApt, errGetSchedule, errUpdateApt error) *AppointmentAPI {
		t, _ := time.Parse(time.RFC3339, "2022-09-26T09:30:00+08:00")
		appointment := repository.Appointment{
			ID:           1,
			ClientUserID: 1,
			CoachUserID:  1,
			BookingTime:  t,
			Status:       aptStatus,
		}
		schedules := []repository.CoachSchedule{
			{
				ID:          1,
				CoachUserID: 1,
				Timezone:    "Asia/Jakarta",
				Day:         "Monday",
				StartTime:   "07:00AM",
				EndTime:     "10:00AM",
			},
		}

		fdb := &fakeAppointmentDB{
			GetAppointmentByIdFunc: func(ctx context.Context, id int64) (repository.Appointment, error) {
				return appointment, errGetApt
			},
			GetCoachScheduleFunc: func(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error) {
				return schedules, errGetSchedule
			},
			UpdateAppointmentFunc: func(ctx context.Context, a repository.Appointment) error {
				return errUpdateApt
			},
		}

		return NewAppointmentAPI(
			fdb,
			30*time.Minute,
		)
	}

	newReq := func(userType string, userId, aptId int64, time time.Time) RescheduleAppointmentRequest {
		return RescheduleAppointmentRequest{
			UserID:        userId,
			UserType:      userType,
			AppointmentID: uint64(aptId),
			BookingTime:   time,
		}
	}

	t.Run("success", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.Nil(t, err)
	})

	t.Run("error invalid user type", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(CLIENT_USER_TYPE, 1, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.EqualError(t, err, "reschedule only for coach")
	})

	t.Run("error invalid booking time", func(t *testing.T) {
		req := newReq(COACH_USER_TYPE, 1, 1, time.Time{})

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.EqualError(t, err, "invalid booking time")
	})

	t.Run("error db get appointment", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, sql.ErrNoRows, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.ErrorIs(t, err, sql.ErrNoRows)
	})

	t.Run("error appointment already cancel by client", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_CLOSED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.EqualError(t, err, "appointment canceled by client")
	})

	t.Run("error forbidden appointment", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 2, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.EqualError(t, err, "forbidden, appointment not belongs to you")
	})

	t.Run("error db get schedule", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)
		e := errors.New("failed")

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, e, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.ErrorIs(t, err, e)
	})

	t.Run("error schedule not found", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-25T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, nil)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.EqualError(t, err, "not valid slot")
	})

	t.Run("error db update schedule appointment", func(t *testing.T) {
		bookingTime, _ := time.Parse(time.RFC3339, "2022-09-26T10:30:00+08:00")
		req := newReq(COACH_USER_TYPE, 1, 1, bookingTime)
		e := errors.New("failed")

		api := newAPI(APPOINTMENT_STATUS_BOOKED, nil, nil, e)
		err := api.RescheduleAppointment(context.Background(), req)

		assert.ErrorIs(t, err, e)
	})
}
