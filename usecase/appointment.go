package usecase

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/yoratyo/appointment-coach/repository"
)

type AppointmentAPI struct {
	db           AppointmentDB
	slotDuration time.Duration
}

const (
	CLIENT_USER_TYPE = "CLIENT"
	COACH_USER_TYPE  = "COACH"

	APPOINTMENT_STATUS_BOOKED   = "BOOKED"
	APPOINTMENT_STATUS_CANCELED = "CANCELED"
	APPOINTMENT_STATUS_CLOSED   = "CLOSED"

	UTC_TIMEZONE = "UTC"
)

func NewAppointmentAPI(db AppointmentDB, slotDuration time.Duration) *AppointmentAPI {
	return &AppointmentAPI{
		db:           db,
		slotDuration: slotDuration,
	}
}

type AppointmentDB interface {
	GetUserById(ctx context.Context, id int64) (repository.User, error)
	GetCoachSchedule(ctx context.Context, coach_user_id int64) ([]repository.CoachSchedule, error)
	CreateAppointment(ctx context.Context, a repository.Appointment) (uint64, error)
	GetAppointmentById(ctx context.Context, id int64) (repository.Appointment, error)
	UpdateAppointment(ctx context.Context, a repository.Appointment) error
}

type GetUserResponse struct {
	UserID   uint64
	Name     string
	Email    string
	UserType string
}

type MakeAppointmentRequest struct {
	ClientUserID int64
	UserType     string
	CoachUserID  int64
	BookingTime  time.Time
	Notes        string
}

type MakeAppointmentResponse struct {
	AppointmentID uint64 `json:"appointment_id"`
}

type CancelAppointmentRequest struct {
	UserID        int64
	UserType      string
	AppointmentID uint64
}

type RescheduleAppointmentRequest struct {
	UserID        int64
	UserType      string
	AppointmentID uint64
	BookingTime   time.Time
}

func (a *AppointmentAPI) GetUser(ctx context.Context, userId int64) (GetUserResponse, error) {
	user, err := a.db.GetUserById(ctx, userId)
	if err != nil {
		return GetUserResponse{}, fmt.Errorf("db GetUserById: %w", err)
	}

	return GetUserResponse{
		UserID:   user.ID,
		Name:     user.Name,
		Email:    user.Email,
		UserType: user.UserType,
	}, nil
}

func (a *AppointmentAPI) CheckCoachSchedule(ctx context.Context, coachUserId int64, bookingTime time.Time) (bool, error) {
	coachSchedules, err := a.db.GetCoachSchedule(ctx, coachUserId)
	if err != nil {
		return false, fmt.Errorf("db GetCoachSchedule: %w", err)
	}

	var foundSchedule bool

	for _, cs := range coachSchedules {
		loc, err := time.LoadLocation(cs.Timezone)
		if err != nil {
			return false, fmt.Errorf("parsing timezone schedule: %w", err)
		}

		t := bookingTime.In(loc)

		if t.Weekday().String() != cs.Day {
			continue
		}

		timeLayout := "3:04PM"
		check, err := time.Parse(timeLayout, t.Format(timeLayout))
		if err != nil {
			return false, fmt.Errorf("parsing request time: %w", err)
		}
		start, err := time.Parse(timeLayout, cs.StartTime)
		if err != nil {
			return false, fmt.Errorf("parsing start time schedule: %w", err)
		}
		end, err := time.Parse(timeLayout, cs.EndTime)
		if err != nil {
			return false, fmt.Errorf("parsing end time schedule: %w", err)
		}

		if check.After(start.Add(-1*time.Nanosecond)) && check.Add(a.slotDuration).Before(end.Add(1*time.Nanosecond)) {
			foundSchedule = true
			break
		}
	}

	return foundSchedule, nil
}

func (a *AppointmentAPI) MakeAppointment(ctx context.Context, req MakeAppointmentRequest) (MakeAppointmentResponse, error) {
	if req.UserType != CLIENT_USER_TYPE {
		return MakeAppointmentResponse{}, fmt.Errorf("invalid user type, must be client")
	}

	if req.BookingTime.IsZero() {
		return MakeAppointmentResponse{}, fmt.Errorf("invalid booking time")
	}

	foundSchedule, err := a.CheckCoachSchedule(ctx, req.CoachUserID, req.BookingTime)
	if err != nil {
		return MakeAppointmentResponse{}, fmt.Errorf("CheckCoachSchedule: %w", err)
	}

	if !foundSchedule {
		return MakeAppointmentResponse{}, fmt.Errorf("not valid slot")
	}

	loc, err := time.LoadLocation(UTC_TIMEZONE)
	if err != nil {
		return MakeAppointmentResponse{}, fmt.Errorf("parsing timezone UTC: %w", err)
	}
	bookInUTC := req.BookingTime.In(loc)

	id, err := a.db.CreateAppointment(ctx, repository.Appointment{
		ClientUserID: uint64(req.ClientUserID),
		CoachUserID:  uint64(req.CoachUserID),
		BookingTime:  bookInUTC,
		Notes:        req.Notes,
		Status:       APPOINTMENT_STATUS_BOOKED,
	})
	if err != nil {
		return MakeAppointmentResponse{}, fmt.Errorf("db CreateAppointment: %w", err)
	}

	return MakeAppointmentResponse{
		AppointmentID: id,
	}, nil
}

func (a *AppointmentAPI) CancelAppointment(ctx context.Context, req CancelAppointmentRequest) error {
	apt, err := a.db.GetAppointmentById(ctx, int64(req.AppointmentID))
	if err != nil {
		return fmt.Errorf("db GetAppointmentById: %w", err)
	}

	if apt.Status != APPOINTMENT_STATUS_BOOKED {
		return fmt.Errorf("appointment already canceled")
	}

	aptUser := apt.CoachUserID
	tobeStatus := APPOINTMENT_STATUS_CANCELED

	if req.UserType == CLIENT_USER_TYPE {
		aptUser = apt.ClientUserID
		tobeStatus = APPOINTMENT_STATUS_CLOSED
	}

	if aptUser != uint64(req.UserID) {
		return fmt.Errorf("forbidden, appointment not belongs to you")
	}

	err = a.db.UpdateAppointment(ctx, repository.Appointment{
		ID:          req.AppointmentID,
		BookingTime: apt.BookingTime,
		Status:      tobeStatus,
	})
	if err != nil {
		return fmt.Errorf("db UpdateAppointment: %w", err)
	}

	return nil
}

func (a *AppointmentAPI) RescheduleAppointment(ctx context.Context, req RescheduleAppointmentRequest) error {
	if req.UserType == CLIENT_USER_TYPE {
		return fmt.Errorf("reschedule only for coach")
	}

	if req.BookingTime.IsZero() {
		return fmt.Errorf("invalid booking time")
	}

	apt, err := a.db.GetAppointmentById(ctx, int64(req.AppointmentID))
	if err != nil {
		return fmt.Errorf("db GetAppointmentById: %w", err)
	}

	if apt.Status == APPOINTMENT_STATUS_CLOSED {
		return fmt.Errorf("appointment canceled by client")
	}

	if apt.CoachUserID != uint64(req.UserID) {
		return fmt.Errorf("forbidden, appointment not belongs to you")
	}

	foundSchedule, err := a.CheckCoachSchedule(ctx, req.UserID, req.BookingTime)
	if err != nil {
		return fmt.Errorf("CheckCoachSchedule: %w", err)
	}

	if !foundSchedule {
		return fmt.Errorf("not valid slot")
	}

	loc, err := time.LoadLocation(UTC_TIMEZONE)
	if err != nil {
		return fmt.Errorf("parsing timezone UTC: %w", err)
	}
	bookInUTC := req.BookingTime.In(loc)

	err = a.db.UpdateAppointment(ctx, repository.Appointment{
		ID:          req.AppointmentID,
		BookingTime: bookInUTC,
		Status:      APPOINTMENT_STATUS_BOOKED,
	})
	if err != nil {
		return fmt.Errorf("db UpdateAppointment: %w", err)
	}

	return nil
}
