package main

import (
	"context"
	"log"
	"net/http"
	"os/signal"
	"syscall"

	"gitlab.com/yoratyo/appointment-coach/config"
	"gitlab.com/yoratyo/appointment-coach/httpserver"
	"gitlab.com/yoratyo/appointment-coach/repository"
	"gitlab.com/yoratyo/appointment-coach/usecase"
)

func main() {
	pg := repository.MustNewPostgreSQL(
		config.Env.PostgresUser,
		config.Env.PostgresPassword,
		config.Env.PostgresHost,
		config.Env.PostgresDBName,
	)

	srv := httpserver.New(
		usecase.NewAppointmentAPI(pg, config.Env.SlotDuration),
		config.Env.AppPort,
		config.Env.AuthJWTKey,
		config.Env.JWTKeyMaxAge,
	)

	ctx, _ := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)

	shutdownDoneChan := make(chan struct{})
	go func() {
		<-ctx.Done()

		if err := srv.Shutdown(context.Background()); err != nil {
			log.Fatal("Error shutting down HTTP server: " + err.Error())
		}

		log.Println("Gracefull Shutdown")

		close(shutdownDoneChan)
	}()

	log.Println("Server starting listen port", srv.Addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal("Error starting server: ", err)
	}

	<-shutdownDoneChan
}
