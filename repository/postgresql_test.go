package repository

import (
	"context"
	"database/sql"
	"errors"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestGetUserByID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	p := &PostgreSQL{
		Conn: sqlx.NewDb(db, "postgres"),
	}

	expected := User{
		ID:       1,
		Name:     "test",
		UserType: "CLIENT",
	}

	expectedQuery := "SELECT .* FROM users WHERE id = \\$1"

	t.Run("success", func(t *testing.T) {
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected.ID).
			WillReturnRows(sqlmock.NewRows([]string{
				"id", "name", "email", "phone_number", "user_type",
			}).AddRow(
				expected.ID,
				expected.Name,
				expected.Email,
				expected.PhoneNumber,
				expected.UserType,
			))

		res, err := p.GetUserById(context.Background(), int64(expected.ID))

		assert.Nil(t, err)
		assert.Equal(t, expected, res)
	})

	t.Run("error", func(t *testing.T) {
		e := errors.New("unknown err")
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected.ID).
			WillReturnError(e)

		res, err := p.GetUserById(context.Background(), int64(expected.ID))

		assert.Equal(t, User{}, res)
		assert.ErrorIs(t, err, e)
	})
}

func TestGetCoachSchedule(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	p := &PostgreSQL{
		Conn: sqlx.NewDb(db, "postgres"),
	}

	expected := []CoachSchedule{
		{
			ID:          1,
			CoachUserID: 1,
			Timezone:    "UTC",
			Day:         "Monday",
			StartTime:   "01:00PM",
			EndTime:     "03:00PM",
		},
	}

	expectedQuery := "SELECT .* FROM coach_schedules WHERE coach_user_id = \\$1"

	t.Run("success", func(t *testing.T) {
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected[0].ID).
			WillReturnRows(sqlmock.NewRows([]string{
				"id", "coach_user_id", "timezone", "day", "start_time", "end_time",
			}).AddRow(
				expected[0].ID,
				expected[0].CoachUserID,
				expected[0].Timezone,
				expected[0].Day,
				expected[0].StartTime,
				expected[0].EndTime,
			))

		res, err := p.GetCoachSchedule(context.Background(), int64(expected[0].CoachUserID))

		assert.Nil(t, err)
		assert.Equal(t, expected, res)
	})

	t.Run("error no rows", func(t *testing.T) {
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected[0].ID).
			WillReturnRows(sqlmock.NewRows([]string{
				"id", "coach_user_id", "timezone", "day", "start_time", "end_time",
			}))

		res, err := p.GetCoachSchedule(context.Background(), int64(expected[0].CoachUserID))

		assert.Equal(t, sql.ErrNoRows, err)
		assert.Equal(t, []CoachSchedule{}, res)
	})

	t.Run("error", func(t *testing.T) {
		e := errors.New("unknown err")
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected[0].ID).
			WillReturnError(e)

		res, err := p.GetCoachSchedule(context.Background(), int64(expected[0].CoachUserID))

		assert.Equal(t, []CoachSchedule{}, res)
		assert.ErrorIs(t, err, e)
	})
}

func TestCreateAppointment(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	p := &PostgreSQL{
		Conn: sqlx.NewDb(db, "postgres"),
	}

	expectedQuery := "INSERT INTO appointments (.+) RETURNING id"

	t.Run("success", func(t *testing.T) {
		id := uint64(1)
		a := Appointment{
			ClientUserID: 1,
			CoachUserID:  1,
			Status:       "BOOKED",
		}
		mock.ExpectQuery(expectedQuery).
			WithArgs(
				a.ClientUserID,
				a.CoachUserID,
				a.BookingTime,
				a.Notes,
				a.Status,
			).WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(id))

		res, err := p.CreateAppointment(context.Background(), a)

		assert.Nil(t, err)
		assert.Equal(t, id, res)
	})

	t.Run("error returning id", func(t *testing.T) {
		a := Appointment{
			ClientUserID: 1,
			CoachUserID:  1,
			Status:       "BOOKED",
		}
		mock.ExpectQuery(expectedQuery).
			WithArgs(
				a.ClientUserID,
				a.CoachUserID,
				a.BookingTime,
				a.Notes,
				a.Status,
			).WillReturnRows(sqlmock.NewRows([]string{"id"}))

		res, err := p.CreateAppointment(context.Background(), a)

		assert.Equal(t, uint64(0), res)
		assert.ErrorIs(t, err, sql.ErrNoRows)
	})

	t.Run("error query", func(t *testing.T) {
		a := Appointment{}
		e := errors.New("unknown err")
		mock.ExpectQuery(expectedQuery).
			WillReturnError(e)

		res, err := p.CreateAppointment(context.Background(), a)

		assert.Equal(t, uint64(0), res)
		assert.ErrorIs(t, err, e)
	})
}

func TestGetAppointmentById(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	p := &PostgreSQL{
		Conn: sqlx.NewDb(db, "postgres"),
	}

	expected := Appointment{
		ID:           1,
		ClientUserID: 1,
		CoachUserID:  1,
		Status:       "BOOKED",
	}

	expectedQuery := "SELECT .* FROM appointments WHERE id = \\$1"

	t.Run("success", func(t *testing.T) {
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected.ID).
			WillReturnRows(sqlmock.NewRows([]string{
				"id", "client_user_id", "coach_user_id", "booking_time", "notes", "status", "created_at", "updated_at",
			}).AddRow(
				expected.ID,
				expected.ClientUserID,
				expected.CoachUserID,
				expected.BookingTime,
				expected.Notes,
				expected.Status,
				expected.CreatedAt,
				expected.UpdatedAt,
			))

		res, err := p.GetAppointmentById(context.Background(), int64(expected.ID))

		assert.Nil(t, err)
		assert.Equal(t, expected, res)
	})

	t.Run("error", func(t *testing.T) {
		e := errors.New("unknown err")
		mock.ExpectQuery(expectedQuery).
			WithArgs(expected.ID).
			WillReturnError(e)

		res, err := p.GetAppointmentById(context.Background(), int64(expected.ID))

		assert.Equal(t, Appointment{}, res)
		assert.ErrorIs(t, err, e)
	})
}

func TestUpdateAppointment(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	p := &PostgreSQL{
		Conn: sqlx.NewDb(db, "postgres"),
	}

	a := Appointment{
		ID:           1,
		ClientUserID: 1,
		CoachUserID:  1,
		Status:       "BOOKED",
	}

	expectedQuery := regexp.QuoteMeta("UPDATE appointments SET status = $1, booking_time = $2 WHERE id = $3")

	t.Run("success", func(t *testing.T) {
		mock.ExpectExec(expectedQuery).WithArgs(
			a.Status,
			a.BookingTime,
			a.ID,
		).WillReturnResult(sqlmock.NewResult(0, 1))

		err := p.UpdateAppointment(context.Background(), a)

		assert.Nil(t, err)
	})

	t.Run("unknow error", func(t *testing.T) {
		e := errors.New("unknow error")
		mock.ExpectExec(expectedQuery).WillReturnError(e)

		err := p.UpdateAppointment(context.Background(), a)

		assert.NotNil(t, err)
		assert.ErrorIs(t, err, e)
	})
}
