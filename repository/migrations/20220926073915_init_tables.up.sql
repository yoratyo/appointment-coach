CREATE TABLE IF NOT EXISTS users (
    id              bigserial PRIMARY KEY,
    name            varchar(80) NOT NULL,
    email           varchar(50),
    phone_number    varchar(15),
    user_type       varchar(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS coach_schedules (
    id              bigserial PRIMARY KEY,
    coach_user_id   bigint NOT NULL,
    timezone        varchar(80) NOT NULL,
    day             varchar(15) NOT NULL,
    start_time      varchar(10) NOT NULL,
    end_time        varchar(10) NOT NULL
);
CREATE INDEX IF NOT EXISTS coach_user_id_idx ON coach_schedules (coach_user_id);

CREATE TABLE IF NOT EXISTS appointments (
    id              bigserial PRIMARY KEY,
    client_user_id  bigint NOT NULL,
    coach_user_id   bigint NOT NULL,
    booking_time    timestamp with time zone NOT NULL,
    notes           text,
    status          varchar(15) NOT NULL,
    created_at      timestamp with time zone NOT NULL,
    updated_at      timestamp with time zone NOT NULL,
    UNIQUE (coach_user_id, booking_time)
);