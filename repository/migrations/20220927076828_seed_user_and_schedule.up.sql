INSERT INTO users
(id, name, email, phone_number, user_type)
VALUES
(1, 'Christy Schumm', '', '', 'COACH'),
(2, 'Natalia Stanton Jr.', '', '', 'COACH'),
(3, 'Nola Murazik V', '', '', 'COACH'),
(4, 'Elyssa O''Kon', '', '', 'COACH'),
(5, 'Dr. Geovany Keebler', '', '', 'COACH'),
(6, 'Jhony R', '', '', 'CLIENT'),
(7, 'Merry Q', '', '', 'CLIENT');

INSERT INTO coach_schedules
(coach_user_id, timezone, "day", start_time, end_time)
VALUES
(1, 'America/North_Dakota/New_Salem', 'Monday', '9:00AM', '5:30PM'),
(1, 'America/North_Dakota/New_Salem', 'Tuesday', '8:00AM', '4:00PM'),
(1, 'America/North_Dakota/New_Salem', 'Thursday', '9:00AM', '4:00PM'),
(1, 'America/North_Dakota/New_Salem', 'Friday', '7:00AM', '2:00PM'),
(2, 'US/Central', 'Tuesday', '8:00AM', '10:00AM'),
(2, 'US/Central', 'Wednesday', '11:00AM', '6:00PM'),
(2, 'US/Central', 'Saturday', '9:00AM', '3:00PM'),
(2, 'US/Central', 'Sunday', '8:00AM', '3:00PM'),
(3, 'America/Yakutat', 'Monday', '8:00AM', '10:00AM'),
(3, 'America/Yakutat', 'Tuesday', '11:00AM', '1:00PM'),
(3, 'America/Yakutat', 'Wednesday', '8:00AM', '10:00AM'),
(3, 'America/Yakutat', 'Saturday', '8:00AM', '11:00AM'),
(3, 'America/Yakutat', 'Sunday', '7:00AM', '9:00AM'),
(4, 'US/Central', 'Monday', '9:00AM', '3:00PM'),
(4, 'US/Central', 'Tuesday', '6:00AM', '1:00PM'),
(4, 'US/Central', 'Wednesday', '6:00AM', '11:00AM'),
(4, 'US/Central', 'Friday', '8:00AM', '12:00PM'),
(4, 'US/Central', 'Saturday', '9:00AM', '4:00PM'),
(4, 'US/Central', 'Sunday', '8:00AM', '10:00AM'),
(5, 'US/Central', 'Thursday', '7:00AM', '2:00PM'),
(5, 'US/Central', 'Thursday', '3:00PM', '5:00PM');