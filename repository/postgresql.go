package repository

import (
	"context"
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/jmoiron/sqlx"
)

type PostgreSQL struct {
	Conn sqlx.ExtContext
}

//go:embed migrations/*.sql
var migrationsFS embed.FS

func MustNewPostgreSQL(user, pass, host, dbname string) *PostgreSQL {
	dbURL := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable&TimeZone=UTC",
		user, pass, host, dbname,
	)

	sqlDB, err := sql.Open("postgres", dbURL)
	if err != nil {
		log.Fatal(err)
	}

	s, err := iofs.New(migrationsFS, "migrations")
	if err != nil {
		log.Fatal(err)
	}
	m, err := migrate.NewWithSourceInstance("iofs", s, dbURL)
	if err != nil {
		log.Fatal(err)
	}

	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		log.Fatal(err)
	}

	return &PostgreSQL{
		Conn: sqlx.NewDb(sqlDB, "postgres"),
	}
}

type User struct {
	ID          uint64 `db:"id"`
	Name        string `db:"name"`
	Email       string `db:"email"`
	PhoneNumber string `db:"phone_number"`
	UserType    string `db:"user_type"`
}

func (p *PostgreSQL) GetUserById(ctx context.Context, id int64) (User, error) {
	var res User

	q := `SELECT id, name, email, phone_number, user_type FROM users WHERE id = $1`

	if err := p.Conn.QueryRowxContext(ctx, q, id).StructScan(&res); err != nil {
		return User{}, fmt.Errorf("GetUserById - sql select row: %w [query: %s]", err, q)
	}

	return res, nil
}

type CoachSchedule struct {
	ID          uint64 `db:"id"`
	CoachUserID uint64 `db:"coach_user_id"`
	Timezone    string `db:"timezone"`
	Day         string `db:"day"`
	StartTime   string `db:"start_time"`
	EndTime     string `db:"end_time"`
}

func (p *PostgreSQL) GetCoachSchedule(ctx context.Context, coach_user_id int64) ([]CoachSchedule, error) {
	var res []CoachSchedule

	q := `SELECT id, coach_user_id, timezone, day, start_time, end_time FROM coach_schedules WHERE coach_user_id = $1`

	if err := sqlx.SelectContext(ctx, p.Conn, &res, q, coach_user_id); err != nil {
		return []CoachSchedule{}, fmt.Errorf("GetCoachSchedule - sql exec: %w [query: %s] [args: %d]", err, q, coach_user_id)
	}

	if len(res) == 0 {
		return []CoachSchedule{}, sql.ErrNoRows
	}

	return res, nil
}

type Appointment struct {
	ID           uint64    `db:"id"`
	ClientUserID uint64    `db:"client_user_id"`
	CoachUserID  uint64    `db:"coach_user_id"`
	BookingTime  time.Time `db:"booking_time"`
	Notes        string    `db:"notes"`
	Status       string    `db:"status"`
	CreatedAt    time.Time `db:"created_at"`
	UpdatedAt    time.Time `db:"updated_at"`
}

func (p *PostgreSQL) CreateAppointment(ctx context.Context, a Appointment) (uint64, error) {
	q := `INSERT INTO appointments (client_user_id, coach_user_id, booking_time, notes, status, created_at, updated_at) 
		VALUES ($1, $2, $3, $4, $5, NOW(), NOW()) RETURNING id`

	row := p.Conn.QueryRowxContext(ctx, q, a.ClientUserID, a.CoachUserID, a.BookingTime, a.Notes, a.Status)
	if err := row.Err(); err != nil {
		return 0, fmt.Errorf("CreateAppointment - QueryRowxContext error : %w [query: %s]", err, q)
	}

	var id uint64
	if err := row.Scan(&id); err != nil {
		return 0, fmt.Errorf("CreateAppointment - Scan error : %w [query: %s]", err, q)
	}

	return id, nil
}

func (p *PostgreSQL) GetAppointmentById(ctx context.Context, id int64) (Appointment, error) {
	var res Appointment

	q := `SELECT id, client_user_id, coach_user_id, booking_time, notes, status, created_at, updated_at FROM appointments WHERE id = $1`

	if err := p.Conn.QueryRowxContext(ctx, q, id).StructScan(&res); err != nil {
		return Appointment{}, fmt.Errorf("GetUserById - sql select row: %w [query: %s]", err, q)
	}

	return res, nil
}

func (p *PostgreSQL) UpdateAppointment(ctx context.Context, a Appointment) error {
	q := `UPDATE appointments SET status = $1, booking_time = $2 WHERE id = $3`

	if _, err := p.Conn.ExecContext(ctx, q, a.Status, a.BookingTime, a.ID); err != nil {
		return fmt.Errorf("sql exec: %w [query: %s]", err, q)
	}

	return nil
}
